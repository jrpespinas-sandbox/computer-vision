# Computer Vision

These projects cover different applications of deep learning on computer vision. 
It will be implemented using mostly TF 2.0 and some PyTorch. 
The first project I made is using Inception-v3 to classify a certain x-ray with a positive or negative on tuberculosis.
This an image recognition task; future projects will make use of other techniques in computer vision to detect Tuberculosis from Chest Radiographs.


## Directory
1) **Computer Vision: Assumed Pulmonary Tuberculosis Recognition on Postero-Anterior Chest Radiograph** (Undergraduate Thesis)

## Dataset
1) [Tuberculosis Chest X-ray Image Data Sets](https://lhncbc.nlm.nih.gov/publication/pub9931)